package com.example.android.customasynctask

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.android.customasynctask.databinding.ActivityMainBinding
import java.lang.Thread.sleep

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var isStarted = false

    private val asyncTask = object : CustomAsyncTask<Int, Int, String>() {

        override fun onPreExecute() {
            Log.d("GG", "onPreExecute. thread: ${Thread.currentThread().name}")
            setViews("Cancel", "", true, View.VISIBLE)
        }

        override fun doInBackground(params: Int): String {
            for (i in 0 until params) {
                publishProgress(i)
                sleep(1000)
                Log.d("GG", "doInBackground. thread: ${Thread.currentThread().name}")
            }
            return params.toString()
        }

        override fun onProgressUpdate(progress: Int) {
            Log.d("GG", "onProgressUpdate. thread: ${Thread.currentThread().name}")
            binding.progressIndicator.progress = progress
        }

        override fun onPostExecute(result: String) {
            handler.post {
                Log.d("GG", "onPostExecute. thread: ${Thread.currentThread().name}")
                setViews("Start", "$result seconds have passed", false, View.GONE)
            }
        }

        override fun cancel() {
            super.cancel()
            setViews("Start", "Timer was canceled", false, View.GONE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater).also { setContentView(it.root) }

        binding.timerButton.setOnClickListener {
            if (isStarted) {
                asyncTask.cancel()
            } else {
                val seconds = binding.editText.text.toString().toInt()
                binding.progressIndicator.max = seconds
                asyncTask.execute(seconds)
            }
        }
    }

    fun setViews(textButton: String, textResult: String, isStarted: Boolean, visibility: Int) {
        binding.apply {
            timerButton.text = textButton
            this@MainActivity.isStarted = isStarted
            resultTextView.text = textResult
            progressIndicator.progress = 0
            progressIndicator.visibility = visibility
        }
    }
}