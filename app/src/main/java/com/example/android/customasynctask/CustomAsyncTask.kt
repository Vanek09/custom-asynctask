package com.example.android.customasynctask

import android.os.Handler
import android.os.Looper

abstract class CustomAsyncTask<Params, Progress, Result> {

    private val backgroundThread = BackgroundThread()
    val handler: Handler = Handler(Looper.getMainLooper())

    abstract fun onPreExecute()
    abstract fun doInBackground(params: Params): Result
    abstract fun onProgressUpdate(progress: Progress)
    abstract fun onPostExecute(result: Result)

    fun publishProgress(progress: Progress) {
        handler.post {
            onProgressUpdate(progress)
        }
    }

    fun execute(params: Params) {
        handler.post { onPreExecute() }
        backgroundThread.execute {
            val result = runCatching { doInBackground(params) }.getOrNull()
            result?.let { onPostExecute(it) }
        }
    }

    open fun cancel() {
        backgroundThread.cancel()
    }

    class BackgroundThread {

        private lateinit var thread: Thread

        fun execute(task: () -> Unit): BackgroundThread {
            thread = object : Thread() {
                override fun run() {
                    task.invoke()
                }
            }
            thread.start()
            return this
        }

        fun cancel() {
            thread.interrupt()
        }
    }
}